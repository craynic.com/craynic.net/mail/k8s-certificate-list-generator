import "reflect-metadata";
import * as k8s from '@kubernetes/client-node';
import {CertificateListGenerator} from "./CertificateListGenerator"
import {container} from "tsyringe";
import {Logger} from "./Logger";
import {createK8sConfig} from "./createK8sConfig";
import {config} from "dotenv";

// enable ENV variables
config({path: __dirname + '/../.env'});

container.register(k8s.KubeConfig, {useFactory: createK8sConfig});
container.registerInstance('k8s-label-selector', process.env['LABEL_SELECTOR']);
container.registerInstance('logger-label', 'generator');

// create generator
const generator: CertificateListGenerator = container.resolve(CertificateListGenerator),
    logger: Logger = container.resolve(Logger);

generator.getList().then((result: Array<string>) => {
    console.log(result.join("\n"));
});

const watchPromise: Promise<void> = generator.watch();

watchPromise.then(() => logger.info('Finished.'));
