import {Certificate} from "./Certificate";
import * as k8s from "@kubernetes/client-node";
import forge from "node-forge";
import {injectable} from "tsyringe";

@injectable()
export class CertificateFingerprintResolver {
    private static readonly tlsCertDataKey: string = 'tls.crt';

    public constructor(private kubeConfig: k8s.KubeConfig) {
    }

    public async getFingerprint(certificate: Certificate): Promise<string> {
        const secretName: string = certificate.spec.secretName,
            namespace: string = certificate.metadata!.namespace!;

        const api: k8s.CoreV1Api = this.kubeConfig.makeApiClient(k8s.CoreV1Api);

        return await api.readNamespacedSecret(secretName, namespace).then(({body}): string => {
            if (body.data == undefined) {
                throw 'Invalid secret';
            }

            if (!body.data.hasOwnProperty(CertificateFingerprintResolver.tlsCertDataKey)) {
                throw 'Invalid secret';
            }

            const tlsCert: string = body.data[CertificateFingerprintResolver.tlsCertDataKey];
            const decodedCert = Buffer.from(tlsCert, 'base64').toString().trim();

            if (decodedCert === '') {
                return '';
            }

            return this.calculateFingerprint(decodedCert);
        });
    }

    private calculateFingerprint(certificateString: string): string {
        const cert: forge.pki.Certificate = forge.pki.certificateFromPem(certificateString),
            der = forge.asn1.toDer(forge.pki.certificateToAsn1(cert)).getBytes(),
            messageDigest: forge.md.MessageDigest = forge.md.sha256.create();

        messageDigest.update(der);

        return messageDigest.digest().toHex().replace(/(.{2}(?!$))/g, '$1:').toUpperCase();
    }
}
