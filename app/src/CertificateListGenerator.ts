import * as k8s from '@kubernetes/client-node';
import {Certificate} from "./Certificate";
import {ListPromise} from "@kubernetes/client-node";
import {KubernetesListObject} from "@kubernetes/client-node/dist/types";
import http = require('http');
import {CertificateFingerprintResolver} from "./CertificateFingerprintResolver";
import {inject, injectable} from "tsyringe";
import {Logger} from "./Logger";

@injectable()
export class CertificateListGenerator {
    private static readonly apiGroup: string = 'cert-manager.io';
    private static readonly customObjectPlural: string = 'certificates';

    public watchPromise: Promise<void>|null = null;

    constructor(
        private kubeConfig: k8s.KubeConfig,
        private logger: Logger,
        private fingerprintResolver: CertificateFingerprintResolver,
        @inject("k8s-label-selector") private labelSelector?: string
    ) {
    }

    public async getCertificates(): Promise<{response: http.IncomingMessage, body: KubernetesListObject<Certificate>;}> {
        const api: k8s.CustomObjectsApi = this.kubeConfig.makeApiClient(k8s.CustomObjectsApi);

        return await api.listClusterCustomObject(
            CertificateListGenerator.apiGroup,
            // TODO: get list of possible versions or the preferred version
            'v1alpha3',
            CertificateListGenerator.customObjectPlural,
            undefined,
            undefined,
            undefined,
            this.labelSelector
        ).then((res): {response: http.IncomingMessage, body: KubernetesListObject<Certificate>} => {
            return res as {response: http.IncomingMessage, body: KubernetesListObject<Certificate>};
        });
    }

    public async getList(): Promise<Array<string>> {
        const listItemsResponse: {body: KubernetesListObject<Certificate>} = await this.getCertificates(),
            items: Array<Certificate> = listItemsResponse.body.items;

        return Promise.all(
            items.map(
                async (certificate: Certificate): Promise<string> => {
                    const fingerprint: string = await this.fingerprintResolver.getFingerprint(certificate),
                        labelValue: string|undefined = (this.labelSelector === undefined)
                            ? ''
                            : certificate.metadata!.labels![this.labelSelector];

                    return `${fingerprint} ${labelValue}`;
                }
            )
        );
    }

    public async watch(): Promise<void> {
        const listFunction: ListPromise<Certificate> = () => this.getCertificates();

        const informer: k8s.Informer<any> = k8s.makeInformer(
            this.kubeConfig,
            `/apis/${CertificateListGenerator.apiGroup}/v1alpha3/${CertificateListGenerator.customObjectPlural}`,
            listFunction
        );

        informer.on('add', (obj: Certificate) => this.onCertificateAdded(obj));
        informer.on('update', (obj: Certificate) => this.onCertificateUpdated(obj));
        informer.on('delete', (obj: Certificate) => this.onCertificateDeleted(obj));
        informer.on('error', (obj: Certificate) => this.onCertificateError(obj));

        this.watchPromise = informer.start();

        await this.watchPromise;
    }

    private onCertificateAdded(certificate: Certificate): void {
        this.logger.info('Added certificate', {details: {name: certificate.metadata!.name}});
    }

    private onCertificateUpdated(certificate: Certificate): void {
        this.logger.info('Updated certificate', {details: {name: certificate.metadata!.name}});
    }

    private onCertificateDeleted(certificate: Certificate): void {
        this.logger.info('Deleted certificate', {details: {name: certificate.metadata!.name}});
    }

    private onCertificateError(error: any): void {
        this.logger.error('Error', {details: error});
    }
}
