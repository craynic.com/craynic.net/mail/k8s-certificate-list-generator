import * as k8s from "@kubernetes/client-node";

export declare class Certificate implements k8s.KubernetesObject {
    apiVersion?: string;
    kind?: string;
    metadata?: k8s.V1ObjectMeta;

    spec: {
        commonName: string,
        issuerRef: {
            kind: string,
            name: string
        },
        secretName: string
    }
}
