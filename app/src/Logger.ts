import * as winston from "winston";
import {inject, injectable} from "tsyringe";

function createLogger(loggerLabel: string): winston.Logger {
    const myFormat: winston.Logform.Format = winston.format.printf(
        ({level, message, label, timestamp, details}) =>
            `${timestamp} [${label}] ${level}: ${message} ${details !== undefined ? JSON.stringify(details) : ''}`
    );

    return winston.createLogger({
        level: process.env['LOG_LEVEL'] ?? 'info',
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.label({label: loggerLabel}),
            winston.format.prettyPrint(),
            winston.format.colorize(),
            myFormat
        ),
        transports: new winston.transports.Console(),
    });
}

@injectable()
export class Logger {
    private logger: winston.Logger;

    constructor(@inject("logger-label") loggerLabel: string) {
        this.logger = createLogger(loggerLabel);
    }

    public info(message: string, ...meta: any[]) {
        this.logger.info(message, ...meta);
    }

    public error(message: string, ...meta: any[]) {
        this.logger.info(message, ...meta);
    }
}