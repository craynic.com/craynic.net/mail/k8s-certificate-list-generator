#!/usr/bin/env bash

set -Eeuo pipefail

FINGERPRINTS_FILE="/opt/fingerprints/fingerprints"
DAEMONIZE="false"
SLEEP_INTERVAL=15

ts() {
  while IFS= read -r line; do
    printf '[%s] %s\n' "$(date '+%Y-%m-%d %H:%M:%S')" "$line";
  done
}

usage() {
  echo "Invalid command-line arguments."
  echo ""
  echo "Valid arguments are:"
  echo "  -d              Daemonize; defaults to false"
  echo "  -f <filename>   Target fingerprint filename; defaults to \"$FINGERPRINTS_FILE\""
  echo "  -l <selector>   Alternative label selector to use; default is to use the LABEL_SELECTOR env variable"
  echo "  -s <interval>   Alternative sleep interval; defaults to \"$SLEEP_INTERVAL\""
}

while getopts ":df:l:s:" o; do
  case "${o}" in
    d)
      DAEMONIZE="true"
      ;;
    f)
      FINGERPRINTS_FILE="$OPTARG"
      ;;
    l)
      LABEL_SELECTOR="$OPTARG"
      ;;
    s)
      SLEEP_INTERVAL="$OPTARG"
      ;;
    *)
      usage
      exit 1
  esac
done

if [[ -z "$LABEL_SELECTOR" ]]; then
  echo "Either the LABEL_SELECTOR environment variable needs to be set, or the\
 label selector provided with -l command line argument." >/dev/stderr
  exit 1
fi

getAllFingerprints() {
  CERT_JSON_PATH="{range .items[*]}{.metadata.namespace}:{.spec.secretName}:{.status.conditions[0].status},{end}"
  CERTS="$(kubectl get certificate -A -l "$LABEL_SELECTOR" -o jsonpath="$CERT_JSON_PATH")"

  if [[ "$CERTS" == "" ]]; then
    return
  fi

  echo "$CERTS" | tr "," "\n" | grep -v "^$" | grep ":True$" | while read -r CERT_FULL; do
    CERT_NS="$(echo "$CERT_FULL" | cut -d":" -f1)"
    SECRET_NAME="$(echo "$CERT_FULL" | cut -d":" -f2)"
    TLS_CERT="$(kubectl get secret -n "$CERT_NS" -o jsonpath='{.data.tls\.crt}' -- "$SECRET_NAME" | base64 -d)"
    CERT_FINGERPRINT="$(echo "$TLS_CERT" | openssl x509 -noout -fingerprint -sha256 | cut -d"=" -f2)"
    CERT_SUBJECT="$(echo "$TLS_CERT" | openssl x509 -noout -subject)"

    echo "$CERT_FINGERPRINT $CERT_SUBJECT"
  done
}

updateFingerprints() {
  FINGERPRINTS="$(getAllFingerprints | sort)"

  DIFF=$(echo -n "$FINGERPRINTS" | diff -d --suppress-common-lines -- "$FINGERPRINTS_FILE" "-" | grep "^[<>]" || true)

  if [[ "$DIFF" != "" ]]; then
    echo "$DIFF" | cut -d" " -f1,2 | sed "s/^>/Adding certificate/" | sed "s/^</Removing certificate/"

    echo -n "$FINGERPRINTS" > "$FINGERPRINTS_FILE"
  fi
}

# create the file if it does not exist
touch -- "$FINGERPRINTS_FILE"

updateFingerprints | ts

if [[ "$DAEMONIZE" != "true" ]]; then
  echo "Job finished." | ts
  exit 0
fi

echo "Running as a deamon" | ts

while true; do
  updateFingerprints | ts

  sleep "$SLEEP_INTERVAL"
done
